package com.open.gateway.resolver;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Auther: tanhang
 * @Date: 2019/4/4 14:03
 * @Description: 用户维度限流
 */
public class UserKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        return  Mono.just(exchange.getRequest().getQueryParams().getFirst("user"));
    }
}
