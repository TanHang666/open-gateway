package com.open.gateway.resolver;

import org.springframework.cloud.gateway.filter.ratelimit.KeyResolver;
import org.springframework.context.annotation.Bean;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

/**
 * @Auther: tanhang
 * @Date: 2019/4/4 14:00
 * @Description: 根据uri去限流
 */
public class UriKeyResolver implements KeyResolver {
    @Override
    public Mono<String> resolve(ServerWebExchange exchange) {
        System.out.println(exchange.getRequest().getURI().getPath());
        return Mono.just(exchange.getRequest().getURI().getPath());
    }

}
