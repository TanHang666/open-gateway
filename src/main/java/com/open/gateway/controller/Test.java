package com.open.gateway.controller;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @Auther: tanhang
 * @Date: 2019/4/3 18:51
 * @Description:
 */
@RestController
@RequestMapping(value = "/test")
public class Test {
    @GetMapping(value = "/sayHi")
    public String sayHi(@RequestParam String name){
        return name +" sayHi!";
    }

}
