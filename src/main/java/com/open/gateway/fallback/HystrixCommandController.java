package com.open.gateway.fallback;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

/**
 * @Auther: tanhang
 * @Date: 2019/4/2 16:08
 * @Description:
 */
@RestController
public class HystrixCommandController {

    @RequestMapping("/fallback")
    public Mono<String> fallback() {
        return Mono.just("网关错误 error");
    }

   /* @HystrixCommand(fallbackMethod = "serviceHiFallbackcmd")
    public void serviceHiFallbackcmd(){
        System.out.println("断路由触发了！");
    }*/
}
